%% brute force iterating all values .. no cross validation

clear all;
%load the dataset
load('boston.mat');

gammaPowerValues = -16:-4;
gamma = (ones(size(gammaPowerValues))*2) .^ gammaPowerValues;

sigmaPowerValues = 3:12;
sigma = (ones(size(sigmaPowerValues))*2) .^ sigmaPowerValues;

indicies = randperm(506);

%trainingSetSize = 2/3;
trainSet = boston(indicies(1:337),1:13);
%testSetSize = 1/3;
testSet = boston(indicies(338:end),1:13);

yTrain = boston(indicies(1:337),14);
yTest = boston(indicies(338:end),14);

figure,

hold on;
gammaIndex = 1;
sigmaIndex = 1;
counter = 1;
for sigmaIndex = 1:length(sigma)
   for gammaIndex = 1:length(gamma)
        
    trainOnes = ones(size(trainSet,1),1);
    testOnes = ones(size(testSet,1),1); 

%     X = [trainSet trainOnes];
  X = trainSet;

    K = zeros(size(X,1),size(X,1));
    for i = 1:size(X,1)
        for j = 1:size(X,1)
            K(i,j) = exp(-1* ((norm(X(i,:) - X(j,:)))^2)/(2*sigma(sigmaIndex)^2));
        end
    end

    alpha = kridgereg(K, yTrain, gamma(gammaIndex));
    cost = dualcost(K,yTrain,alpha);
    
    plotvals(counter) = cost;
    
    % create test kernel and compute cost function
    l = size(K,1);
    
    %K = zeros(size(X,1),size(X,1));
%     Ktest = zeros(size(X,1),size(testSet,1));
%     yyTest = zeros(1,length(testSet));
%     for i = 1:l
%         for j = 1:size(testSet,1)
%             Ktest(i,j) = exp(-1* ((norm(X(i,:) - testSet(j,:)))^2)/(2*sigma(sigmaIndex)^2));
%         end
%         yyTest = yyTest + Ktest(i,:)*alpha(i);
%     end
   
    KtestTrain = zeros(size(testSet,1),size(X,1));
    for i = 1:size(KtestTrain,1)
        for j = 1:size(KtestTrain,2)
            KtestTrain(i,j) = exp(-1* ((norm(testSet(i,:) - X(j,:)))^2)/(2*sigma(sigmaIndex)^2));
        end
        
    end
  
    
     cost2 = dualcost(KtestTrain,boston(indicies(338:end),14),alpha);
     plotvals2(counter) = cost2;
    counter = counter + 1;
    
   end
end
plot(plotvals, '*r');
plot(plotvals2, '*b');
hold off;


%% With Cross Validation

clear all;
%load the dataset
load('boston.mat');

gammaPowerValues = -16:-4;
gamma = (ones(size(gammaPowerValues))*2) .^ gammaPowerValues;

sigmaPowerValues = 3:12;
sigma = (ones(size(sigmaPowerValues))*2) .^ sigmaPowerValues;

indicies = randperm(506);

%trainingSetSize = 2/3;
trainSet = boston(indicies(1:337),1:13);
%testSetSize = 1/3;
testSet = boston(indicies(338:end),1:13);

yTrain = boston(indicies(1:337),14);
yTest = boston(indicies(338:end),14);

figure,

hold on;
gammaIndex = 1;
sigmaIndex = 1;
counter = 1;
for sigmaIndex = 1:length(sigma)
   for gammaIndex = 1:length(gamma)
        
    trainOnes = ones(size(trainSet,1),1);
    testOnes = ones(size(testSet,1),1); 

%     X = [trainSet trainOnes];
  X = trainSet;

    K = zeros(size(X,1),size(X,1));
    for i = 1:size(X,1)
        for j = 1:size(X,1)
            K(i,j) = exp(-1* ((norm(X(i,:) - X(j,:)))^2)/(2*sigma(sigmaIndex)^2));
        end
    end

    alpha = kridgereg(K, yTrain, gamma(gammaIndex));
    cost = dualcost(K,yTrain,alpha);
    
    plotvals(counter) = cost;
    
    % create test kernel and compute cost function
    l = size(K,1);
    
    %K = zeros(size(X,1),size(X,1));
%     Ktest = zeros(size(X,1),size(testSet,1));
%     yyTest = zeros(1,length(testSet));
%     for i = 1:l
%         for j = 1:size(testSet,1)
%             Ktest(i,j) = exp(-1* ((norm(X(i,:) - testSet(j,:)))^2)/(2*sigma(sigmaIndex)^2));
%         end
%         yyTest = yyTest + Ktest(i,:)*alpha(i);
%     end
   
    KtestTrain = zeros(size(testSet,1),size(X,1));
    for i = 1:size(KtestTrain,1)
        for j = 1:size(KtestTrain,2)
            KtestTrain(i,j) = exp(-1* ((norm(testSet(i,:) - X(j,:)))^2)/(2*sigma(sigmaIndex)^2));
        end
        
    end
  
    
     cost2 = dualcost(KtestTrain,boston(indicies(338:end),14),alpha);
     plotvals2(counter) = cost2;
    counter = counter + 1;
    
   end
end
plot(plotvals, '*r');
plot(plotvals2, '*b');
hold off;