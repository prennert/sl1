%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Least Squares Regression (LSR) - data dimensionality %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Part a

data_dim = 10;
train_set_dim = 100;
test_set_dim = 100;

% generate random w
w = randn(data_dim,1);

% generate training and test set samples
x_train = zeros(train_set_dim, data_dim);
y_train = zeros(train_set_dim, 1);
x_test  = zeros(test_set_dim, data_dim);
y_test = zeros(test_set_dim, 1);
[x_train, x_test, y_train, y_test] = generateData(w, data_dim, train_set_dim, test_set_dim);

% store data for later use
save('ex2a_data', 'w', 'x_train', 'y_train', 'x_test', 'y_test');

%% Part b

% estimate new weight
w_star = regression(x_train, y_train, 0);

% compute mean squared error for new weights
err_train = meanSquaredError(w_star, x_train, y_train);
err_test = meanSquaredError(w_star, x_test, y_test);

%% Part c

small_train_set_dim = 10;

% generate a 10 sample subset of the existing training set;
% w and and the training set remain the same
idx = randperm(train_set_dim);
x_train_small = x_train(idx(1:small_train_set_dim),:);
y_train_small = y_train(idx(1:small_train_set_dim));

% estimate new weight
w_star_small = regression(x_train_small, y_train_small, 0);

% compute mean squared errors
err_train_small = meanSquaredError(w_star_small, x_train_small, y_train_small);
err_test_small = meanSquaredError(w_star_small, x_test, y_test);

% store data for later use
save('ex2a_data', '-append', 'x_train_small', 'y_train_small');


%% Part d

[avg_err_train, avg_err_test, avg_err_train_small, avg_err_test_small] = ...
    LSR_n_times(100, data_dim, train_set_dim, test_set_dim, small_train_set_dim);
