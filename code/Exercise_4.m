%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ridge Regression - setting the regularization parameter %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Part a

% load data from exercise 2
%load('ex2a_data');

% generate data
data_dim = 10;
train_set_dim = 100;
test_set_dim = 100;
w = randn(data_dim,1);
[x_train, x_test, y_train, y_test] = generateData(w, data_dim, train_set_dim, test_set_dim);

% find optimal gamma
gamma_range = -6:3;
[gamma, train_set_err, test_set_err] = ...
    find_best_gamma(gamma_range, x_train, y_train, x_test, y_test, 1);


%% Part b

% create subsample set
idx = randperm(train_set_dim);
small_train_set_dim = 10;
x_train_small = x_train(idx(1:small_train_set_dim),:);
y_train_small = y_train(idx(1:small_train_set_dim));

% find optimal gamma
[gamma_small, small_train_set_err, small_test_set_err] = ...
    find_best_gamma(gamma_range, x_train_small, y_train_small, x_test, y_test, 1);


%% Plot both training set errors together
% figure(3);
% hold on;
% semilogy(gamma_range, train_set_err, '*b');
% semilogy(gamma_range, test_set_err, '*m');
% semilogy(gamma_range, small_train_set_err, 'ob');
% semilogy(gamma_range, small_test_set_err, 'or');
% legend('Training set error for 100 training samples', 'Test set error for 100 training samples','Training set error for 10 training samples', 'Test set error for 10 training samples');
% hold off;
