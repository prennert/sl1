%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ridge Regression - tuning the regularization parameter %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Part a

% generate data
data_dim = 10;
train_set_dim = 100;
test_set_dim = 100;
w = randn(data_dim,1);
[x_train_full, x_test, y_train_full, y_test] = generateData(w, data_dim, train_set_dim, test_set_dim);

% split training set into training and validation sets
x_train = x_train_full(1:80,:);
x_validation = x_train_full(81:100,:);
y_train = y_train_full(1:80);
y_validation = y_train_full(81:100);

% find optimal gamma on validation set
gamma_range = -6:3;
[gamma, train_set_err, validation_set_err, test_set_err] = ...
    find_best_gamma_with_validation(gamma_range, x_train, y_train, ...
    x_validation, y_validation, x_test, y_test, 1);

% perform ridge regression on whole training set with chosen gamma
w_star = regression(x_train_full, y_train_full, gamma); 


%% Part b

% create subsample set
idx = randperm(train_set_dim);
small_train_set_dim = 10;
x_train_full_small = x_train_full(idx(1:small_train_set_dim),:);
y_train_full_small = y_train_full(idx(1:small_train_set_dim));

% split training set into training and validation sets
x_train_small = x_train_full_small(1:8,:);
x_validation_small = x_train_full_small(9:10,:);
y_train_small = y_train_full_small(1:8);
y_validation_small = y_train_full_small(9:10);

% find optimal gamma on validation set
[gamma_small, train_set_err_small, validation_set_err_small, test_set_err_small] = ...
    find_best_gamma_with_validation(gamma_range, x_train_small, y_train_small, ...
    x_validation_small, y_validation_small, x_test, y_test, 1);

% perform ridge regression on whole training set with chosen gamma
w_star_small = regression(x_train_full_small, y_train_full_small, gamma_small); 

%% Part c

% The regularization parameter is usually larger for the smaller training
% set because given the tendency for overfitting, the regularization
% parameter will bias the result towards the data more than the noise.
% (To be edited in the report ...)

%% Part d

% gamma will be small because the dimension of the data is small in
% comparison with the sample set size, therefore there is no risk of
% overfitting; also, the reange of the errors will not vary as much as for
% multidimensional data; in some cases the test set errors will be almost
% equal for all values of gamma

% generate one dimensional data
data_dim = 1;
train_set_dim = 100;
test_set_dim = 100;
w = randn(data_dim,1);
[x_train_full, x_test, y_train_full, y_test] = generateData(w, data_dim, train_set_dim, test_set_dim);

% split training set into training and validation sets
x_train = x_train_full(1:80,:);
x_validation = x_train_full(81:100,:);
y_train = y_train_full(1:80);
y_validation = y_train_full(81:100);

% find optimal gamma on validation set
gamma_range = -6:3;
[gamma, train_set_err, validation_set_err, test_set_err] = ...
    find_best_gamma_with_validation(gamma_range, x_train, y_train, ...
    x_validation, y_validation, x_test, y_test, 1);

% perform ridge regression on whole training set with chosen gamma
w_star = regression(x_train_full, y_train_full, gamma); 


