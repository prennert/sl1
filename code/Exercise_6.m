%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ridge Regression - cross validation tuning %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Part a

k = 5;

% perform training with reduced training set and validate using validation
% set. Retain validation scores for each gamma and compute an average of
% them per gamma. the one with the lowest such average is the sought for
% gamma. perform ridge regression using this and compute test set error.

% generate data
data_dim = 10;
train_set_dim = 100;
test_set_dim = 100;
w = randn(data_dim,1);
[x_train_full, x_test, y_train_full, y_test] = generateData(w, data_dim, train_set_dim, test_set_dim);
gamma_range = -6:3;

gamma = cross_validation(gamma_range, k, x_train_full, y_train_full, x_test, y_test, 1);

%% Part b

% Cross validation with a sample set of 10.

% create subsample set
idx = randperm(train_set_dim);
small_train_set_dim = 10;
x_train_full_small = x_train_full(idx(1:small_train_set_dim),:);
y_train_full_small = y_train_full(idx(1:small_train_set_dim));

gamma_small = cross_validation(gamma_range, k, x_train_full_small, y_train_full_small, x_test, y_test, 1);

