%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tuning regularization parameter method comparison %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Generate 100 data sets and compare regularization parameter tuning
% methods on them

n_times = 100;

test_err_t = zeros(n_times,1); % for tuning using training set error minimisation
test_err_v = zeros(n_times,1); % for tuning using a validation set
test_err_c = zeros(n_times,1); % for tuning using cross validation

for i=1:100
    
    % generate data set
    data_dim = 10;
    train_set_dim = 100;
    test_set_dim = 100;
    w = randn(data_dim,1);
    [x_train_full, x_test, y_train_full, y_test] = generateData(w, data_dim, train_set_dim, test_set_dim);
    
    gamma_range = -6:3;
    
    %% tune gamma using training set error minimization
    [gamma, train_set_err, test_set_err] = ...
        find_best_gamma(gamma_range, x_train_full, y_train_full, x_test, y_test,0);
    
    % perform ridge regression and record test set error
    w_star = regression(x_train_full, y_train_full, gamma);
    test_err_t(i) = meanSquaredError(w_star, x_test, y_test);
    
    %% tune gamma using 20% validation set and 80% training set
   
    % split training set into training and validation sets
    x_train = x_train_full(1:80,:);
    x_validation = x_train_full(81:100,:);
    y_train = y_train_full(1:80);
    y_validation = y_train_full(81:100);

    % find optimal gamma on validation set
    [gamma, train_set_err, validation_set_err] = ...
        ... %find_best_gamma(gamma_range, x_train, y_train, x_validation, y_validation,0);
        find_best_gamma_with_validation(gamma_range, x_train, y_train, x_validation, y_validation,x_test, y_test,0);
    % perform ridge regression and record test set error
    w_star = regression(x_train_full, y_train_full, gamma);
    test_err_v(i) = meanSquaredError(w_star, x_test, y_test);
    
    %% tune gamma with cross validation
    k = 5; %no. folds
    gamma = cross_validation(gamma_range, k, x_train_full, y_train_full, x_test, y_test, 0);
    
    % perform ridge regression and record test set error
    w_star = regression(x_train_full, y_train_full, gamma);
    test_err_c(i) = meanSquaredError(w_star, x_test, y_test);
end

% compute mean and standard deviation of error for each case
mean_t = mean(test_err_t);
std_t = std(test_err_t);
mean_v = mean(test_err_v);
std_v = std(test_err_v);
mean_c = mean(test_err_c);
std_c = std(test_err_c);

disp(['Minimizing training ser error: mean=' num2str(mean_t) ' std=' num2str(std_t)]);
disp(['Validation set: mean=' num2str(mean_v) ' std=' num2str(std_v)]);
disp(['Cross-validation: mean=' num2str(mean_c) ' std=' num2str(std_c)]);

