% CRIM - per capita crime rate by town
% ZN - proportion of residential land zoned for lots over 25,000 sq.ft.
% INDUS - proportion of non-retail business acres per town.
% CHAS - Charles River dummy variable (1 if tract bounds river; 0 otherwise)
% NOX - nitric oxides concentration (parts per 10 million)
% RM - average number of rooms per dwelling
% AGE - proportion of owner-occupied units built prior to 1940
% DIS - weighted distances to five Boston employment centres
% RAD - index of accessibility to radial highways
% TAX - full-value property-tax rate per $10,000
% PTRATIO - pupil-teacher ratio by town
% B - 1000(Bk - 0.63)^2 where Bk is the proportion of blacks by town
% LSTAT - % lower status of the population
% MEDV - Median value of owner-occupied homes in $1000's

%% Naive Regression

clear all;
%load the dataset
load('boston.mat');

trainErrorSum = 0;
testErrorSum = 0;

for iter = 1:20
    
indicies = randperm(506);

%trainingSetSize = 2/3;
trainSet = boston(indicies(1:337),1:13);
%testSetSize = 1/3;
testSet = boston(indicies(338:end),1:13);

trainOnes = ones(size(trainSet,1),1);
testOnes = ones(size(testSet,1),1); 


yTrain = boston(indicies(1:337),14);
yTest = boston(indicies(338:end),14);

w_star = regression(trainOnes, yTrain, 0);

trainErrorSum = trainErrorSum + meanSquaredError(w_star, trainOnes, yTrain);
testErrorSum = testErrorSum + meanSquaredError(w_star, testOnes, yTest);

end

trainErrorSum = trainErrorSum / iter;
testErrorSum = testErrorSum / iter;

disp('y = b');
disp(strcat('Average Train Error: ', num2str(trainErrorSum)));
disp(strcat('Average Test Error: ', num2str(testErrorSum)));

figure,
hold on;
plot(trainErrorSum, 'or');
plot(testErrorSum, 'ob');
hold off;

%% Linear Regression with single attributes
clear all;
%load the dataset
load('boston.mat');

for attributeIter = 1:13
    
    trainErrorSum = 0;
    testErrorSum = 0;
    
    for iter = 1:20
    indicies = randperm(506);

    %trainingSetSize = 2/3;
    trainSet = boston(indicies(1:337),1:13);
    %testSetSize = 1/3;
    testSet = boston(indicies(338:end),1:13);

    trainOnes = ones(size(trainSet,1),1);
    testOnes = ones(size(testSet,1),1); 


    yTrain = boston(indicies(1:337),14);
    yTest = boston(indicies(338:end),14);

    w_star = regression([trainSet(:,attributeIter) trainOnes], yTrain, 0);

    trainErrorSum = trainErrorSum + meanSquaredError(w_star, [trainSet(:,attributeIter) trainOnes], yTrain);
    testErrorSum = testErrorSum + meanSquaredError(w_star, [testSet(:,attributeIter) testOnes], yTest);
    end
    
    finalTrainErrorSum(attributeIter) = trainErrorSum / iter;
    finalTestErrorSum(attributeIter) = testErrorSum / iter;

%     disp(strcat('attribute index: ', num2str(attributeIter)));
%     disp(strcat('Average Train Error: ', num2str(finalTrainErrorSum(attributeIter))));
%     disp(strcat('Average Test Error: ', num2str(finalTestErrorSum(attributeIter))));
    disp(strcat(num2st, num2str(finalTrainErrorSum(attributeIter))));
end

hold on;
plot(finalTrainErrorSum, '*b');
plot(finalTestErrorSum, '*r');
hold off;

disp('Average of single attributes');
disp(strcat('Average Train Error: ', num2str(mean(finalTrainErrorSum))));
disp(strcat('Average Test Error: ', num2str(mean(finalTestErrorSum))));






%% Linear Regression with all attributes

clear all;
%load the dataset
load('boston.mat');

trainErrorSum = 0;
testErrorSum = 0;

for iter = 1:20

indicies = randperm(506);

%trainingSetSize = 2/3;
trainSet = boston(indicies(1:337),1:13);
%testSetSize = 1/3;
testSet = boston(indicies(338:end),1:13);

trainOnes = ones(size(trainSet,1),1);
testOnes = ones(size(testSet,1),1); 


yTrain = boston(indicies(1:337),14);
yTest = boston(indicies(338:end),14);

w_star = regression([trainSet trainOnes], yTrain, 0);

trainErrorSum = trainErrorSum + meanSquaredError(w_star, [trainSet trainOnes], yTrain);
testErrorSum = testErrorSum + meanSquaredError(w_star, [testSet testOnes], yTest);

end

trainErrorSum = trainErrorSum / iter;
testErrorSum = testErrorSum / iter;

disp('y = wx+b with all attributes');
disp(strcat('Average Train Error: ', num2str(trainErrorSum)));
disp(strcat('Average Test Error: ', num2str(testErrorSum)));

hold on;
plot(trainErrorSum, 'sr');
plot(testErrorSum, 'sb');
hold off;

legend('Naive Regression Training Error', ...
    'Naive Regression Testing Error', ...
    'Regression Individual Training Error', ...
    'Regression Individual Testing Error', ...
    'Regression All Training Error', ...
    'Regression All Testing Error');



