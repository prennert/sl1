%  Function for running Least Square Error algorithm many times.
%  It creates a new sample set for every run.
 % Input
    % n_times: how many times to repeat
    % data_dim: dimension of the data point
    % train_set_dim: size of the training set
    % test_set_dim: size of the test set
    % train_set_small_dim: size of the subsample training set
 % Ouput
    % avg_err_train: average mean squared error on large training set
    % avg_err_test: average mean squared error on test set using a large training set
    % avg_err_train_small: average mean squared error on small training set 
    % avg_err_test_small: average mean squared error on test set using a small training set

function [avg_err_train, avg_err_test, avg_err_train_small, avg_err_test_small] = ...
    LSR_n_times(n_times, data_dim, train_set_dim, test_set_dim, train_set_small_dim)

% setup error vectors
err_train = zeros(n_times, 1);
err_test = zeros(n_times, 1);
err_train_small = zeros(n_times, 1);
err_test_small = zeros(n_times, 1);

for i=1:100
    % generate random w
    w = randn(data_dim,1);

    % generate training and test set samples
    x_train = zeros(train_set_dim, data_dim);
    y_train = zeros(train_set_dim, 1);
    x_test  = zeros(test_set_dim, data_dim);
    y_test = zeros(test_set_dim, 1);
    [x_train, x_test, y_train, y_test] = generateData(w, data_dim, train_set_dim, test_set_dim);
    
    % generate a 10 sample subset of the existing training set;
    % w and and the training set remain the same
    idx = randperm(train_set_dim);
    x_train_small = x_train(idx(1:train_set_small_dim),:);
    y_train_small = y_train(idx(1:train_set_small_dim));
    
    % estimate new weights
    w_star = regression(x_train, y_train, 0);
    w_star_small = regression(x_train_small, y_train_small, 0);

    % compute mean squared error for new weights
    err_train(i) = meanSquaredError(w_star, x_train, y_train);
    err_test(i) = meanSquaredError(w_star, x_test, y_test);
    err_train_small(i) = meanSquaredError(w_star_small, x_train_small, y_train_small);
    err_test_small(i) = meanSquaredError(w_star_small, x_test, y_test);
end

% average error vectors
avg_err_train = mean(err_train);
avg_err_test =  mean(err_test);
avg_err_train_small = mean(err_train_small);
avg_err_test_small =  mean(err_test_small);

end