% Function for performing k-fold cross validation on a data set.
% Input
    % gamma_range: range of powers of 10 for the values of gamma
    % k: number of required validations
    % x_train_full: matrix of training points that will be split up into
    %               training and validation sets
    % y_train_full: vector of training labels that will be split up into
    %               training and validation sets
    % x_test: matrix of test points 
    % y_test: vector of test labels 
    % plot : set to 0 if no plotting is required and 1 otherwise
 % Ouput
    % gamma: the optimal regularization parameter from the given range
   
%%


function gamma = ...
    cross_validation(gamma_range, k, x_train_full, y_train_full, x_test, y_test, plot)


l = size(x_train_full, 1);
valid_set_len = l /k;
g_l = length(gamma_range);
validation_set_err = zeros(k,g_l);

for j = 1:k
    % create training and validation sets
    i = (j-1)*valid_set_len + 1;
    x_validation = x_train_full(i : (i+valid_set_len-1), :);
    y_validation = y_train_full(i : (i+valid_set_len-1));
    x_train = x_train_full;
    y_train = y_train_full;
    x_train(i : (i+valid_set_len-1), :) = [];
    y_train(i : (i+valid_set_len-1)) = [];
    
    % get training, validation and test set errors     
    [dc, dc1, validation_set_err(j,:), dc2] = ...
    find_best_gamma_with_validation(gamma_range, x_train, y_train, ...
    x_validation, y_validation, x_test, y_test, 0);
end

% calculate cross-validation score which is the average of the validation
%error on all sets for each gamma

cv_scores = mean(validation_set_err);

[val, gamma_idx] = min(cv_scores);

gamma = 10^gamma_range(gamma_idx);

if (plot)
    % calculate training and test set errors for each gamma
    [dc, train_err, test_err] = find_best_gamma(gamma_range, x_train_full, y_train_full,...
        x_test, y_test,0);
    
    % plot cross-validation score, training and test set error again gamma
    figure();
    hold on;
    semilogy(gamma_range, cv_scores', '*r');
    semilogy(gamma_range, train_err, '*b');
    semilogy(gamma_range, test_err, '*m');
    title([ 'Cross-validation with ' num2str(size(x_train_full,1)) ' training samples: best gamma = 10^' (num2str(gamma_range(gamma_idx))) ]);
    legend('Cross-validation score', 'Training set error', 'Test set error', 'Location', 'NorthOutside');
    hold off;
end

end