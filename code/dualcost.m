% Function for calculating the mean squared error of the dual weights
% Input
    % K: kernel matrix
    % y: label vector
    % alpha: vector of dual weights
 % Ouput
    % error: mean squared error
   
%%

function  error = dualcost(K,y,alpha)

l = size(K,1);
error = (1/l)*(K*alpha - y)'*(K*alpha - y);

end