% Function for finding the best regularization parameter for ridge regression
% by choosing the gamma correpsponding to the minimum training set error.
 % Input
    % gamma_range: range of powers of 10 for the values of gamma
    % x_train: matrix of training points 
    % y_train: vector of training labels 
    % x_test: matrix of test points 
    % y_test: vector of test labels 
    % plot : set to 0 if no plotting is required and 1 otherwise
 % Ouput
    % gamma: the optimal regularization parameter from the given range
    % train_set_err: the training set errors corresponding to the different
    %                values of gamma
    % test_set_err: the test set errors corresponding to the different
    %                values of gamma
   
%%

function [gamma, train_set_err, test_set_err] = ...
    find_best_gamma(gamma_range, x_train, y_train, x_test, y_test, plot)

% gamma takes as values all the powers of 10 between given by the
% gamma_range vector
l = length(gamma_range);
train_set_err = zeros(l,1);
test_set_err = zeros(l,1);

for i = 1:l
    g = 10^gamma_range(i);
    w_star = regression(x_train, y_train, g);
    train_set_err(i) = meanSquaredError(w_star, x_train, y_train);
    test_set_err(i) = meanSquaredError(w_star, x_test, y_test);    
end

% the optimal gamma is the one corresponding to the smallest training set error
[min_err, gamma_idx] = min(train_set_err);
gamma = 10^(gamma_range(gamma_idx));

% plot errors versus gamma if necessary
if (plot)
    figure();
    hold on;
    semilogy(gamma_range, train_set_err, '*b');
    semilogy(gamma_range, test_set_err, '*m');
    title([ num2str(size(x_train,1)) ' training samples: gamma = 10^' (num2str(gamma_range(gamma_idx))) ]);
    legend('Training set error for ', 'Test set error for ');
    hold off;
end

end