% Function for generating random training ans test data. 
 % Input
    % w : weight vector of size [data_dim, 1]
    % data_dim : the dimension of the data point
    % train_set_dim : the length of the training set
    % test_set_dim : the length of the test set
 % Ouput
    % x_train: matrix of training points of size [train_set_dim, data_dim]
    % y_train: vector of training labels of size [train_set_dim, 1]
    % x_test: matrix of test points of size [test_set_dim, data_dim]
    % y_test: vector of test labels [test_set_dim, 1]
%%

function [x_train, x_test, y_train, y_test]...
    = generateData(w, data_dim, train_set_dim, test_set_dim)

% generate random data from the normal distribution
total_set_dim = train_set_dim + test_set_dim;
x = randn(total_set_dim, data_dim);
n = randn(total_set_dim,1);

% create noisy samples
y = x*w + n;

% separate data randomly into training set and test set
idx = randperm(total_set_dim);
train_idx = idx(1:train_set_dim);
test_idx = idx(train_set_dim+1:total_set_dim);
x_train = x(train_idx,:);
y_train = y(train_idx);
x_test = x(test_idx,:);
y_test = y(test_idx);
end