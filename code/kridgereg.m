% Function for performing kernel ridge regression.
% Input
    % K: kernel matrix
    % y: label vector
    % gamma: regularization parameter
 % Ouput
    % alpha: vector of dual weights
   
%%

function alpha = kridgereg(K, y, gamma)

l = size(K,1);
alpha = (K + gamma*eye(l))\y; 

end