% Function for computing the mean squared error
% Input
    % x : data point matrix
    % y : labels vector
    % w : weights vector
 % Output
    % err : mean squared error

function err = meanSquaredError(w, x, y)

l = size(x,1);
err = (w'*x'*x*w - 2*y'*x*w + y'*y) / l;
end
