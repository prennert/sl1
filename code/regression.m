% Function that computes the weight vector w by using either Least Squares
% (gamma = 0) or Ridge Regression (gamma != 0)
 % Input
    % x : data point matrix
    % y : labels vector
    % gamma : regularization factor;
 % Output
    % w: weight vector

function w = regression(x, y, gamma)

l = size(x,1);
w = (x'*x + gamma * l * eye(size(x,2))) \ x'*y;
end