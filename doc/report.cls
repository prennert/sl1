% ------------------------------------------------------------

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{report} % Unser Style
\LoadClass[12pt,a4paper, onecolumn]{IEEEtran} % Papierformat und Dokumenttyp
% -------------------------------------------------------------
% Zun�chst ein paar n�tzliche Packages:
\usepackage[english]{babel} % Deutsche Sprache (neue Rechtschreibung)
\usepackage[T1]{fontenc} % Zeichensatz mit Umlauten
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx} % Grafiken einbinden
\usepackage{moreverb} % Verbatim-Umgebung
\usepackage{cite}	  %Zitate
\usepackage{hyperref} %links inside the document
\usepackage{xcolor}		%colors
\usepackage{pxfonts}
\usepackage{epstopdf}
\usepackage{subfig}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{booktabs}
\usepackage{grffile}	%manage file extension
\usepackage{accents}	% accents :)
\usepackage{float}

% -------------------------------------------------------------

% -------------------------------------------------------------
%---hyperref costumation
% -------------------------------------------------------------
\definecolor{gray} {rgb}{0.4, 0.4, 0.6}
\hypersetup{
    bookmarks=true,         % show bookmarks bar?
    unicode=false,          % non-Latin characters in Acrobat�s bookmarks
    pdftoolbar=true,        % show Acrobat�s toolbar?
    pdfmenubar=true,        % show Acrobat�s menu?
    pdffitwindow=false,     % window fit to page when opened
    pdfstartview={FitH},    % fits the width of the page to the window
    pdftitle={},    % title
    pdfauthor={Christina Amati, Jacques Cali, Peter Rennert},     % author
    pdfsubject={Subject},   % subject of the document
    pdfcreator={Creator},   % creator of the document
    pdfproducer={Producer}, % producer of the document
    pdfkeywords={keywords}, % list of keywords
    pdfnewwindow=true,      % links in new window
    colorlinks=true,       % false: boxed links; true: colored links
    linkcolor=gray,          % color of internal links
    citecolor=green,        % color of links to bibliography
    filecolor=magenta,      % color of file links
    urlcolor=cyan           % color of external links
}

% -------------------------------------------------------------
%---Special commands
% -------------------------------------------------------------
%
\newcommand{\mat}[1]{\textbf{#1}}
\newcommand{\var}{\emph}
\newcommand{\func}{\texttt}
 
 
% Highlight, use hl{text}
\usepackage{soul}


% -------------------------------------------------------------
%--- Listings
% -------------------------------------------------------------
%
\usepackage{listings}

%define colours for the listings
\definecolor{DarkGreen}{rgb}{0.0,0.5,0.0}
\definecolor{LightGray}{rgb}{0.7,0.7,0.7}
\definecolor{CodeBackground}{rgb}{0.95,0.95,0.95}
\definecolor{code}{rgb}{0.1,0.1,0.1}
\definecolor{string}{rgb}{0.7,0.0,0.8}
\definecolor{comment}{rgb}{0.13,1,0.13}
\definecolor{keyword}{rgb}{0.1,0.1,1}

%%%%%%%%%%%%% listing settings
\lstset{
language=Matlab,%
%
%% font settings
basicstyle=\fontsize{10}{12.0}\ttfamily\color{code},	% normal code style
keywordstyle=\color{keyword},							% keyword style
commentstyle=\color{DarkGreen}, 							% comment style
stringstyle=\color{string},
numbers=left,
%
%% environment settings
xleftmargin=0.3in,
backgroundcolor=\color{CodeBackground},
framesep=0.1in,
frame=single,
tabsize=4,
rulesepcolor=\color{LightGray},
captionpos=t
}
%%%%%%%%%%%%%%%%%%%%%

% Und fertig.
\endinput
% -------------------------------------------------------------